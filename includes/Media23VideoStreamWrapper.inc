<?php

/**
 *  @file includes/MediaYouTubeStreamWrapper.inc
 *
 *  Create a YouTube Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance
 */
class Media23VideoStreamWrapper extends MediaReadOnlyStreamWrapper {

  // Overrides $base_url defined in MediaReadOnlyStreamWrapper.
  protected $base_url = '/video/';
  function __constract()
  {
    parent::__constract();
    $base_url = variable_get('video23service_site_url', '') . $base_url;
  }
  /**
   * Returns a url in the format "http://youtube.com/watch?v=qsPQN4MiTeE".
   *
   * Overrides interpolateUrl() defined in MediaReadOnlyStreamWrapper.
   * This is an exact copy of the function in MediaReadOnlyStreamWrapper,
   * here in case that example is redefined or removed.
   */
  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return $this->base_url . '' . $parameters['video'];
    }
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/23video';
  }

  function getTarget($f) {
    return FALSE;
  }

  /**
  * @todo Clear code
  */
  function getOriginalThumbnailPath() {
  	$parts = $this->get_parameters();

    $connector = Connector23Video::getInstance();
    $info = $connector->doPhotoList($parts['video']);
    
    return variable_get('video23service_site_url', '') . '' . $info['photo']['medium_download'];
  }


  /**
  * @todo Clear code
  */
  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    // There's no need to hide thumbnails, always use the public system rather
    // than file_default_scheme().
    $local_path = 'public://media-23video/' . check_plain($parts['video']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());

      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
    }
    return $local_path;
  }
}
