<?php

/**
 * @file includes/MediaInternet23VideoHandler.inc
 *
 * Contains MediaInternet23VideoHandler.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternet23VideoHandler extends MediaInternetBaseHandler {
  /**
   * Check if a YouTube video id is valid.
   *
   * Check against the oembed stream instead of the gdata api site to
   * avoid "yt:quota too_many_recent_calls" errors.
   *
   * @return
   *   Boolean.
   */
  public function parse($embedCode) {
    $patterns = array(
      '@23video\.com/video/([^"\&\? ]+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      // @TODO: Parse is called often. Refactor so that valid ID is checked
      // when a video is added, but not every time the embedCode is parsed.
      if (isset($matches[1])/* && self::validId($matches[1])*/) {
        return file_stream_wrapper_uri_normalize('23video://video/' . $matches[1]);
      }
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    if (empty($file->fid) && $info = $this->getOEmbed()) {
      $file->filename = truncate_utf8($info['photo']['title'], 255);
    }

    return $file;
  }

  /**
   * Returns information about the media. See http://www.oembed.com/.
   *
   * @return
   *   If oEmbed information is available, an array containing 'title', 'type',
   *   'url', and other information as specified by the oEmbed standard.
   *   Otherwise, NULL.
   */
  public function getOEmbed() {
    $uri = $this->parse($this->embedCode);
    $video_id = arg(1, file_uri_target($uri));


    $connector = Connector23Video::getInstance();
    $info = $connector->doPhotoList($video_id);

    return $info;
      
  }
  
}
